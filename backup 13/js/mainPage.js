// Code for the main app page (Past Runs list).


window.addEventListener("load", function() { window.scrollTo(0, 1); });

displayRuns();
var currentRunIndex,i;
var runIndexArray = [];

function displayRuns()
{
	var mainDiv = document.getElementById('prevRunDiv');

	var dispDiv = document.getElementById('testList');

	var APP_PREFIX = "monash.mcd4290.bin.runChallengeApp.";

	currentRunIndex =Number(localStorage.getItem(APP_PREFIX+'runIndex'));
	
	

	if(currentRunIndex === null){

		dispDiv.innerHTML = "You have no previous saved Runs!";

	}else{

		i = currentRunIndex-1;
		//console.log("staring index i:"+i);

		while( i===0 || i>0 ){
						
			
			var runObj = getRun(i);
			
			//checking and skipping deleted runs from View Run.
			if(runObj===null || runObj===undefined){
				--i; //skip
				
			// If the run is not deleted continue..
			}else {
				var btn = document.createElement('div');
				mainDiv.appendChild(btn);
				btn.className="divBtn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effec";

				// Set Button Id as the index value for future referencing.
				btn.id=i;

				// Set an onclick attribute to the element that calls the function 
				// createViewPage(currentRunIndex) to create a page for this run Object.

				//btn.setAttribute('onclick','createViewPage('+i+');');
				btn.setAttribute('onclick','createViewPage('+i+'); location.href="viewRun.html";');


				// If the Run is incomplete set the text color to DarkRed.
				if(runObj.runCompletedStatus==="No"){
					//btn.style.background= 'url("images/incomplete.jpg") center / cover';
					//btn.style.background = 'white';
					//btn.style.color ='red';
					btn.style.borderLeft = '5px outset red';
				}else {
					btn.style.borderLeft = '5px outset green';
				}

				// Set the Contents (infomation about the run) on created element.
				btn.innerHTML =

					'    <br><strong><i class="material-icons">place</i>&nbsp&nbsp&nbsp'+runObj.name+
					'</strong> | '+
					runObj.dateTime+"h<BR><BR>"+
					'<b>» Run Time: </b>'+runObj.timeTaken+"<br>"+
					'<b>» Distance Run: </b>'+runObj.distanceRun+" m<br>"+
					'<b>» Average Speed: </b>'+runObj.averageSpeed+" m/s<BR>";

				//decrement counter
				--i;
			}
		}
	}
}

