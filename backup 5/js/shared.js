 //global scope required

var runComplete=false;
var runIndex;
var timeInSec;
var time;


//Stopwatch Function
/* The timer is started when the function is called and continues until the quitRunButton
* (in NewRun.html) is pressed. 
* Displays to the textArea 'TimerArea' in NewRun.html;
*/

function startTimer()
{
	var disp = document.getElementById('TimerArea'),
  //  start = document.getElementById('start'),
    stop = document.getElementById('QuitRunButton'),
//    clear = document.getElementById('clear'),
    seconds = 0, minutes = 0, hours = 0,
    t;

		function add() {
			seconds++;
			if (seconds >= 60) {
				seconds = 0;
				minutes++;
				if (minutes >= 60) {
					minutes = 0;
					hours++;
				}
			}

			time = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + 
				(minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + 
				(seconds > 9 ? seconds : "0" + seconds);
			disp.value = time;
			timer();
		}
		function timer() {
			t = setTimeout(add, 1000);
		}
		timer();

		/* Stop button */
		stop.addEventListener('click', function() {
			clearTimeout(t);
			timeInSec=seconds+(minutes*60)+(hours*3600);
			
		});

		/* Clear button */
	//clear.onclick = function() {
	//		disp.value = "00:00:00";
	//		seconds = 0; minutes = 0; hours = 0;
	//	}
	}
// Shared code needed by the code of all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.bin.runChallengeApp.";

//Run Class Constructor
function run()
{
	this.startingPoint = start;
	this.endingPoint = end;
	this.timeTaken = time;	
	this.runCompleted = runComplete;
	this.distance = (distance-distancetoEnd);
	console.log("distance:" +this.distance);
	console.log("timeinsec:"+timeInSec);
	console.log("spd: "+this.distance/timeInSec);
	this.avgSpeed = this.distance / timeInSec;
	//function calcAvgSpeed(){
	//	this.avgSpeed = (this.distance)/timeSeconds;
	//}
	
	//this.avgSpeed = this.distance/this.timeTaken;
	
	var currentdate = new Date(); 
	this.datetime = currentdate.getDate() + "/"
					+ (currentdate.getMonth()+1)  + "/" 
					+ currentdate.getFullYear() + " @ "  
					+ currentdate.getHours() + ":"  
					+ currentdate.getMinutes();
}


function sendToLocalStorage(runObj)
{
	// Check if the browser supports local storage
	if (typeof(Storage) !== undefined){
		
		// Check if the program is saving for the first time or not
		if (localStorage.getItem("runIndex")===null){	
			// Initialize the local storage for further use.
			runIndex = 0;
			localStorage.setItem("runIndex", runIndex);
		}
		
		// Obtain the run index number.
		var runIndex = localStorage.getItem('runIndex');
		// Stringify the run object.
		var JSONrunObj = new JSON.stringify(runObj);
		// Store the JSON run object in local storage.
		localStorage.setItem(APP_PREFIX+runIndex,JSONrunObj);
		
		// Iterate the run index number
		runIndex++;
		
		// Store it.
		localStorage.setItem('runIndex',runIndex);
		
		
	} else {
		alert("Your browser does not support local storage. Update or change browser.");
	}
}

// This function displays the given message String as a "toast" message at
// the bottom of the screen.  It will be displayed for 2 second, or if the
// number of milliseconds given by the timeout argument if specified.
function displayMessage(message, timeout)
{
    if (timeout === undefined)
    {
        // Timeout argument not specifed, use default.
        timeout = 2000;
    } 

    if (typeof(message) == 'number')
    {
        // If argument is a number, convert to a string.
        message = message.toString();
    }

    if (typeof(message) != 'string')
    {
        console.log("displayMessage: Argument is not a string.");
        return;
    }

    if (message.length == 0)
    {
        console.log("displayMessage: Given an empty string.");
        return;
    }

    var snackbarContainer = document.getElementById('toast');
    var data = {
        message: message,
        timeout: timeout
    };
    if (snackbarContainer && snackbarContainer.hasOwnProperty("MaterialSnackbar"))
    {
        snackbarContainer.MaterialSnackbar.showSnackbar(data);
    }
}



