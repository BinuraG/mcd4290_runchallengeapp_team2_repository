// Code for the Measure Run page.

// Global-scope variable declarations //
var map = null;
var end, start;
var startingPosition;
var infoWindow = null;
var dest_infoWindow = null; //destination infoWindow used in function createNewRun() requires gloabl scope intialization.
var directionsDisplay = null; // google.maps.DirectionsRender Object, used by function drawRunPath(). Requires gloabl scope.

// Map Initialisation callback.  Will be called when Maps API loads.
function initMap() 
{
	// Change Body Color
	document.body.style.backgroundColor='#1f2835';
	
	// Check if GeoLocation services are available
	if (navigator.geolocation) {
		// if available, get the current position of the user...
		 navigator.geolocation.getCurrentPosition(function(position) {

		// startingPosition contains the latitute and longitude from the navigator.geolocation class
		startingPosition = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
		};
		
		// Create a LatLng Object from the startingPosition Coordinates.	 	
		start = new google.maps.LatLng(startingPosition.lat, startingPosition.lng);
		
		// Initialize new map object from Google Maps API
		map = new google.maps.Map(document.getElementById('map'),{
			center: start,
			zoom: 18,
			styles: [
				{elementType: 'geometry', stylers: [{color: '#242f3e'}]},
				{elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
				{elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
				{
				  featureType: 'administrative.locality',
				  elementType: 'labels.text.fill',
				  stylers: [{color: '#d59563'}]
				},
				{
				  featureType: 'poi',
				  elementType: 'labels.text.fill',
				  stylers: [{color: '#d59563'}]
				},
				{
				  featureType: 'poi.park',
				  elementType: 'geometry',
				  stylers: [{color: '#263c3f'}]
				},
				{
				  featureType: 'poi.park',
				  elementType: 'labels.text.fill',
				  stylers: [{color: '#6b9a76'}]
				},
				{
				  featureType: 'road',
				  elementType: 'geometry',
				  stylers: [{color: '#38414e'}]
				},
				{
				  featureType: 'road',
				  elementType: 'geometry.stroke',
				  stylers: [{color: '#212a37'}]
				},
				{
				  featureType: 'road',
				  elementType: 'labels.text.fill',
				  stylers: [{color: '#9ca5b3'}]
				},
				{
				  featureType: 'road.highway',
				  elementType: 'geometry',
				  stylers: [{color: '#746855'}]
				},
				{
				  featureType: 'road.highway',
				  elementType: 'geometry.stroke',
				  stylers: [{color: '#1f2835'}]
				},
				{
				  featureType: 'road.highway',
				  elementType: 'labels.text.fill',
				  stylers: [{color: '#f3d19c'}]
				},
				{
				  featureType: 'transit',
				  elementType: 'geometry',
				  stylers: [{color: '#2f3948'}]
				},
				{
				  featureType: 'transit.station',
				  elementType: 'labels.text.fill',
				  stylers: [{color: '#d59563'}]
				},
				{
				  featureType: 'water',
				  elementType: 'geometry',
				  stylers: [{color: '#17263c'}]
				},
				{
				  featureType: 'water',
				  elementType: 'labels.text.fill',
				  stylers: [{color: '#515c6d'}]
				},
				{
				  featureType: 'water',
				  elementType: 'labels.text.stroke',
				  stylers: [{color: '#17263c'}]
				}
			  ]
		});

		// Initialize infoWindow to display starting position to user.
		infoWindow = new google.maps.InfoWindow({
			map: map,
			position:start,
			content: 'You are here!'
		});
		// Error Handling	 
		}, function() {
			handleLocationError(true, infoWindow, map.getCenter());
		  });
	} else {
	  // Browser doesn't support Geolocation
	  handleLocationError(false, infoWindow, map.getCenter());
	}
	// show errors if geolocation fails
	function handleLocationError(browserHasGeolocation, infoWindow, startingPosition) {
		infoWindow.setPosition(startingPosition);
		infoWindow.setContent(browserHasGeolocation ?
							  'Error: The Geolocation service failed.' :
							  'Error: Your device doesn\'t support geolocation.');
  	}
}

/*Function to create a new destination within 60m and 150m of the user location.
* calculates displacement between the two points, and sets down an infoWIndow at the destination.
*/
var distance;
function createNewRun()
{
	var properDistance = false // jump starting the while loop
	// checking if there are any previous end destination markers open from previous calls to createNewRun().
	if(dest_infoWindow !== null){
		// Removing that marker before creating a new destination.
		dest_infoWindow.setMap(null);
	}

	while (properDistance===false){
		
			// creating a destination LatLng Object by shiting coords by +-0.001 units.
			end = new google.maps.LatLng(
						(startingPosition.lat+(Math.random()*0.001)),
						(startingPosition.lng+(Math.random()*0.001))
			);
			//Calculating the distance between the two positions.
			distance = google.maps.geometry.spherical.computeDistanceBetween(start, end);
			
			//Check if the generated position falls within the requirements.
			if(distance>60 && distance<150){
				properDistance=true;	// set the boolean counter to true.
			}	
		}
		
		// display a new InfoWindow at the destination with the displacement infomation.
		dest_infoWindow = new google.maps.InfoWindow({
			map: map,
			position: end,
			content: '<div style="yellow">Your Destination<br>('+distance.toFixed(0)+'m away)</div>'
		});

	//Invoke function drawRunPath to request walking directions from google.
	drawRunPath();
}


// Function to render the selected path joining the two destinations.
// A request must be made from Google services.
function drawRunPath ()
{
	//check for any paths drawn before, if there are any previous paths, remove them.
	if(directionsDisplay !== null){
		directionsDisplay.setMap(null);
	}
	
	// creating a request object to be sent to google services.
	var requestPath = {
		origin: start,
		destination: end,
		travelMode: google.maps.TravelMode.WALKING
	};
	
	// API obj.s
	var directionsService = new google.maps.DirectionsService();
	directionsDisplay = new google.maps.DirectionsRenderer(
		{preserveViewport: true}); // set to stop map from zooming or panning once directions are rendered.
	
	// Request a path from start -> end, if recieved from google, draw the path onto the Map.
	directionsService.route(requestPath, function(response, status) {
		if(status=== google.maps.DirectionsStatus.OK){
			directionsDisplay.setDirections(response);
			directionsDisplay.setMap(map);

//			var heading = new google.maps.geometry.spherical.computeHeading(start, end);
//			console.log(heading);
//			map.setHeading(heading);
//			map.setTilt(45)
		}else { 	//display error
			alert("Directions Request Failed!");
		}
	});
}

// FUnction called when the start Run button is pressed. Calculates the user location continously
// and displays it as a marker on the map.
var timerUpdateLocation;
var distancetoEnd;
function runInProgress()
{
	
	//update the current location every 2s.
	timerUpdateLocation = setInterval(updateCurrentLocation, 2000); 
	var currentPos;
	var currentPosMarker=null;
	
	function updateCurrentLocation()
	{
		console.log('updated location');
		// Check is geolocation services are available...
		if (navigator.geolocation) {
			 navigator.geolocation.getCurrentPosition(function(position) {

				// position contains the latitute and longitude from the position.coords class
				 var position = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				 // Create LatLng Object to display a marker.
				currentPos = new google.maps.LatLng(position.lat, position.lng);
				 
				// remove previous markers if any
				 if(currentPosMarker !== null){
					 currentPosMarker.setMap(null);
				 }
				//set new Marker
				 currentPosMarker = new google.maps.Marker({
					 position: currentPos,
					 map: map,
					 title: 'Current Position'
        		});
				 
				 // Check if the user is close to the destination (within 10m)
				 distancetoEnd = google.maps.geometry.spherical.computeDistanceBetween(currentPos, end);
				 
				 // Tell the user the remaining distance.
				 ////* Maybe I should remove this. Testing purposes only.
				 document.getElementById('QuitRunButton').innerHTML = "<b><I>QUIT RUN </I>-</b> Just "+distancetoEnd.toFixed(1)+"m to go! <I>Don't give up!</I>"
				 
				 // if the user has reached the destination, invoke quitRun() function
				 if(distancetoEnd<=10){
					 // simulate the click of the quit button.
					 document.getElementById('QuitRunButton').click(); 
					 runComplete = true;
				 }
			});
		}
	}
	
	}


// Invoking the CreateNewRun() method when the "Create a new Destination" Button is pressed.
document.getElementById('createDestinationButton').addEventListener('click',
function()
{
	createNewRun();
});


// When the Start RUN button is pressed...
document.getElementById('RUNButton').addEventListener('click', 
function()
{

	
	// Check if a destination has been set before starting the Run...
	if(dest_infoWindow !== null){
			
			//change body color back to white.
			document.body.style.backgroundColor='#fff'; 
		
			// Invoke runInProgress() to keep track of user position.
			runInProgress();
		
			//hide the starting location infoWindow.
			infoWindow.setMap(null);

			//hide current buttons.
			document.getElementById('newRunButtonDiv').style.display='none';

			// The size of the map is reduced to compensate for the new interface.
			document.getElementById('map').style.height='calc(100vh - 185px)';

			//Enabling the new interface StartedRunningDiv. (Quit button and timer)
			document.getElementById('startedRunningDiv').style.display='inline-block';

			//Resetting and calling the stopwatch function startTimer() to initiate.
			document.getElementById('TimerArea').value="00:00:00";
			startTimer();	//initiate stopwatch.

			//Toast Message to the user.
			displayMessage("Your Run will automatically end when you are close to your destination!",4000)

	}else{
		// Dispaly a messege asking the user to select a destination first.
		displayMessage("ERROR! Create a Destination before starting a new Run!!")
	}
});


//If the Quit RUN button is pressed... 
document.getElementById('QuitRunButton').addEventListener('click',
function()
{
	
	
	// Stop tracking user location
	clearInterval(timerUpdateLocation);
	// Change Body Color
	document.body.style.backgroundColor='#1f2835';
	// display infoCard
	document.getElementById('runInfoCard').style.display='block';
	
	//disable the inditerminate running bar
	document.getElementById('runningBar').style.display='none';
	
	// Remove the current interface
	document.getElementById('startedRunningDiv').style.display = 'none';
	
	// Remove the Map
	document.getElementById('map').style.display = 'none';
	
	// Remove the header
	document.getElementById('startNewRunHeaderDiv').style.display='none';
	
	// Check if the user has completed the run or if he/she has quit before reaching the end.
	
	// Creating a new Run Object
	newRun = new run();
	
	// Display info about the run...
	
	document.getElementById('runInfo').innerHTML=
	
		'RUN <b>SUMMARY <i class="material-icons">directions_run</i><i class="material-icons">flag</i></b><br>'+newRun.datetime+
		"<br><ul>"+
		//"<li><b>Starting Coordinates:</b>    "+newRun.startingPoint+"</li>"+
		//"<li><b>Destination Coordinates:</b> "+newRun.endingPoint+"</li>"+
		"<li><b>Total Run Time:</b> "+newRun.timeTaken+"</li>"+
		"<li><b>Run Completed?:</b> "+newRun.runCompleted+"</li>"+
		"<li><b>Distance Run:</b> "+newRun.distance.toFixed(1)+"m</li>"+
		"<li><b>Average Speed:</b> "+newRun.avgSpeed+"m/s</li></ul>";
		
	

});

//document.getElementById('saveRunButton').onclick() = 

