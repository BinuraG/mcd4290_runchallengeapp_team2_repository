//Run Class Constructor
function run(start, end, time, timeSeconds, completeStatus, quitPos)
{
	this.startingPoint = start;
	this.endingPoint = end;
	this.timeTaken = time;	
	this.runCompleted = completeStatus;
	this.quitLocation = quitPos;

	function calcDistance(){
		if(completeStatus===true){
			 this.distance = google.maps.geometry.spherical.computeDistanceBetween(start, end);
		}else{
			this.distance = google.maps.geometry.spherical.computeDistanceBetween(start, quitPos);
		}
	}
	
	
	//this.avgSpeed = this.distance/this.timeTaken;
	
	var currentdate = new Date(); 
	this.datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" ;
}


//Stopwatch Function
/* The timer is started when the function is called and continues until the quitRunButton
* (in NewRun.html) is pressed. 
* Displays to the textArea 'TimerArea' in NewRun.html;
*/
var time; //global scope required
function startTimer()
{
	var disp = document.getElementById('TimerArea'),
  //  start = document.getElementById('start'),
    stop = document.getElementById('QuitRunButton'),
//    clear = document.getElementById('clear'),
    seconds = 0, minutes = 0, hours = 0,
    t;

		function add() {
			seconds++;
			if (seconds >= 60) {
				seconds = 0;
				minutes++;
				if (minutes >= 60) {
					minutes = 0;
					hours++;
				}
			}

			time = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + 
				(minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + 
				(seconds > 9 ? seconds : "0" + seconds);
			disp.value = time;
			timer();
		}
		function timer() {
			t = setTimeout(add, 1000);
		}
		timer();

		/* Stop button */
		stop.addEventListener('click', function() {
			clearTimeout(t);
		});

		/* Clear button */
	//clear.onclick = function() {
	//		disp.value = "00:00:00";
	//		seconds = 0; minutes = 0; hours = 0;
	//	}
	}
// Shared code needed by the code of all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];



// This function displays the given message String as a "toast" message at
// the bottom of the screen.  It will be displayed for 2 second, or if the
// number of milliseconds given by the timeout argument if specified.
function displayMessage(message, timeout)
{
    if (timeout === undefined)
    {
        // Timeout argument not specifed, use default.
        timeout = 2000;
    } 

    if (typeof(message) == 'number')
    {
        // If argument is a number, convert to a string.
        message = message.toString();
    }

    if (typeof(message) != 'string')
    {
        console.log("displayMessage: Argument is not a string.");
        return;
    }

    if (message.length == 0)
    {
        console.log("displayMessage: Given an empty string.");
        return;
    }

    var snackbarContainer = document.getElementById('toast');
    var data = {
        message: message,
        timeout: timeout
    };
    if (snackbarContainer && snackbarContainer.hasOwnProperty("MaterialSnackbar"))
    {
        snackbarContainer.MaterialSnackbar.showSnackbar(data);
    }
}

