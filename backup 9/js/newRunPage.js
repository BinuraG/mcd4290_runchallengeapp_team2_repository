// Code for the Measure Run page.

// Global-scope variable declarations //
var map = null;
var infoWindow = null;
var dest_infoWindow = null; //destination infoWindow used in function createNewRun() requires gloabl scope intialization.
var directionsDisplay = null; // google.maps.DirectionsRender Object, used by function drawRunPath(). Requires gloabl scope.

var newRun;

// Map Initialisation callback.  Will be called when Maps API loads.
function initMap() 
{
	
	// If the page load is a Re-Run attempt from the ViewRun Page...
	var isReRun = localStorage.getItem('isReRun');
	
	if(isReRun==="1"){
		alert("yoyo");
		// disable the random location generation button
		document.getElementById('createDestinationButton').disabled = true;

		// Retrieve the CurrentIndex of the selected run from local storage.
		var APP_PREFIX = "monash.mcd4290.bin.runChallengeApp.";
		var currentIndex = localStorage.getItem(APP_PREFIX+'.currentIndex');

		// Retrieve the Run information from Local Storage
		var run = getRun(currentIndex);
		var start = new google.maps.LatLng(run.starting_lat, run.starting_lng);
		var end = new google.maps.LatLng(run.ending_lat, run.ending_lng);
		
		// Initialize a new Run instance of the Run class and set it's properties to the re-attempted Run's properties.
		newRun = new Run();
		newRun.setStartingPoint(start);
		newRun.setEndingPoint(end);
		var startingCoords = {
			lat: run.starting_lat,
			lng: run.starting_lng
		}
		newRun.setStartingCoordinates(startingCoords);
		
		var endingCoords = {
			lat: run.ending_lat,
			lng: run.ending_lng
		}
		newRun.setEndingCoordinates(endingCoords);

	}else if(isReRun==="0") // if the Run is a fresh run create new Run Instance.
	{
		alert("skippin");
		newRun = new Run();
	}
	
	// Change Body Color
	document.body.style.backgroundColor='#1f2835';
	
	// Check if GeoLocation services are available
	if (navigator.geolocation) {
		// if available, get the current position of the user...
		 navigator.geolocation.getCurrentPosition(function(position) {

		// startingPosition contains the latitute and longitude from the navigator.geolocation class
		var startingPosition = {
				lat: position.coords.latitude,
				lng: position.coords.longitude,
				accuracy: position.coords.accuracy
		};
		
		// Checking if the accuracy is below 20m and disablig the New Destination Button otherwise...
	//	if(startingPosition.accuracy>25){
			// disable the buttons
	//		document.getElementById('createDestinationButton').disabled = true;
	//		document.getElementById('RUNButton').disabled = true;
			
	//		displayMessage('Your location accuracy is insufficient (Accuracy: '+(startingPosition.accuracy).toFixed(1)+'m) ...Trying again. Please Wait',6500);
			// Reload Location and retry in 6s.
	//		setTimeout(reloadPage, 6000);

	//	}
	 
		// Create a LatLng Object from the startingPosition Coordinates.	 	
		var start = new google.maps.LatLng(startingPosition.lat, startingPosition.lng);
			 
		if(isReRun==="0"){
			newRun.setStartingCoordinates(startingPosition);
			newRun.setStartingPoint(start);
		}
		
		// Initialize new map object from Google Maps API
		map = new google.maps.Map(document.getElementById('map'),{
			center: start,
			zoom: 18,
			styles:[
						{elementType: 'geometry', stylers: [{color: '#242f3e'}]},
						{elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
						{elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
						{
						  featureType: 'administrative.locality',
						  elementType: 'labels.text.fill',
						  stylers: [{color: '#d59563'}]
						},
						{
						  featureType: 'poi',
						  elementType: 'labels.text.fill',
						  stylers: [{color: '#d59563'}]
						},
						{
						  featureType: 'poi.park',
						  elementType: 'geometry',
						  stylers: [{color: '#263c3f'}]
						},
						{
						  featureType: 'poi.park',
						  elementType: 'labels.text.fill',
						  stylers: [{color: '#6b9a76'}]
						},
						{
						  featureType: 'road',
						  elementType: 'geometry',
						  stylers: [{color: '#38414e'}]
						},
						{
						  featureType: 'road',
						  elementType: 'geometry.stroke',
						  stylers: [{color: '#212a37'}]
						},
						{
						  featureType: 'road',
						  elementType: 'labels.text.fill',
						  stylers: [{color: '#9ca5b3'}]
						},
						{
						  featureType: 'road.highway',
						  elementType: 'geometry',
						  stylers: [{color: '#746855'}]
						},
						{
						  featureType: 'road.highway',
						  elementType: 'geometry.stroke',
						  stylers: [{color: '#1f2835'}]
						},
						{
						  featureType: 'road.highway',
						  elementType: 'labels.text.fill',
						  stylers: [{color: '#f3d19c'}]
						},
						{
						  featureType: 'transit',
						  elementType: 'geometry',
						  stylers: [{color: '#2f3948'}]
						},
						{
						  featureType: 'transit.station',
						  elementType: 'labels.text.fill',
						  stylers: [{color: '#d59563'}]
						},
						{
						  featureType: 'water',
						  elementType: 'geometry',
						  stylers: [{color: '#17263c'}]
						},
						{
						  featureType: 'water',
						  elementType: 'labels.text.fill',
						  stylers: [{color: '#515c6d'}]
						},
						{
						  featureType: 'water',
						  elementType: 'labels.text.stroke',
						  stylers: [{color: '#17263c'}]
						}
					]
		});
			 	if(isReRun==="1"){
					drawRunPath();
					checkReAttemptLocationValidity();
				}
			 
			 
		// Initialize infoWindow to display starting position to user.
		infoWindow = new google.maps.InfoWindow({
			map: map,
			position:start,
			content: 'You are here!'
		});
		// Error Handling	 
		}, function() {
			handleLocationError(true, infoWindow, map.getCenter());
		  });
	} else {
	  // Browser doesn't support Geolocation
	  handleLocationError(false, infoWindow, map.getCenter());
	}
	// show errors if geolocation fails
	function handleLocationError(browserHasGeolocation, infoWindow, startingPosition) {
		infoWindow.setPosition(startingPosition);
		infoWindow.setContent(browserHasGeolocation ?
							  'Error: The Geolocation service failed.' :
							  'Error: Your device doesn\'t support geolocation.');
  	}
	

}



/*Function to create a new destination within 60m and 150m of the user location.
* calculates displacement between the two points, and sets down an infoWIndow at the destination.
*/

function createNewRun()
{
	var properDistance = false // jump starting the while loop
	// checking if there are any previous end destination markers open from previous calls to createNewRun().
	if(dest_infoWindow !== null){
		// Removing that marker before creating a new destination.
		dest_infoWindow.setMap(null);
	}

	while (properDistance===false){
			
			// Access required values from the newRun Object
			var startingPosition = newRun.getStartingCoordinates();
			var start = newRun.getStartingPoint();
			
			var sC_lat = startingPosition.lat;
			var sC_lng = startingPosition.lng;
			// creating a destination LatLng Object by shiting coords by +-0.001 units.
		
			// Random Location Generation based on + or - 0.001 off the current user location.
			var rand = Math.round(Math.random());
			if (rand%2===1){
				var endingPosition = {
					lat:sC_lat+(Math.random()*0.001),
					lng:sC_lng+(Math.random()*0.001)	
				};
			}else {
				var endingPosition = {
					lat:sC_lat-(Math.random()*0.001),
					lng:sC_lng-(Math.random()*0.001)	
				};
			}

			var end = new google.maps.LatLng(endingPosition);
			
			
			// Call object mutator method to set endingPoint
			newRun.setEndingCoordinates(endingPosition);
			newRun.setEndingPoint(end);
		
			//Calculating the distance between the two positions.
			var distance = google.maps.geometry.spherical.computeDistanceBetween(start, end);
			
			// Call run Obj. mutator to set displacement.
			newRun.setDisplacement(distance);
			//Check if the generated position falls within the requirements.
			if(distance>60 && distance<150){
				properDistance=true;	// set the boolean counter to true.
			}	
		}
		
		// display a new InfoWindow at the destination with the displacement infomation.
		dest_infoWindow = new google.maps.InfoWindow({
			map: map,
			position: end,
			content: '<div style="yellow">Your Destination<br>('+distance.toFixed(0)+'m away)</div>'
		});

	//Invoke function drawRunPath to request walking directions from google.
	drawRunPath();
}


// Function to render the selected path joining the two destinations.
// A request must be made from Google services.
function drawRunPath ()
{
	alert("drawpath() accessed");
	//check for any paths drawn before, if there are any previous paths, remove them.
	if(directionsDisplay !== null){
		directionsDisplay.setMap(null);
	}
	
	var start = newRun.getStartingPoint();
	var end  = newRun.getEndingPoint();
	// creating a request object to be sent to google services.
	var requestPath = {
		origin: start,
		destination: end,
		travelMode: google.maps.TravelMode.WALKING
	};
	
	// API obj.s
	var directionsService = new google.maps.DirectionsService();
	directionsDisplay = new google.maps.DirectionsRenderer({
		preserveViewport: true}); // set to stop map from zooming or panning once directions are rendered.
	
	// Request a path from start -> end, if recieved from google, draw the path onto the Map.
	directionsService.route(requestPath, function(response, status) {
		if(status=== google.maps.DirectionsStatus.OK){
			directionsDisplay.setDirections(response);
			directionsDisplay.setMap(map);
		}else { 	//display error
			alert("Directions Request Failed!");
		}
	});
}

// FUnction called when the start Run button is pressed. Calculates the user location continously
// and displays it as a marker on the map.
var timerUpdateLocation;

function runInProgress()
{
	
	//update the current location every 2s.
	timerUpdateLocation = setInterval(updateCurrentLocation, 1000); 
	var currentPos;
	var currentPosMarker=null;
	var circle = null; // accuracy circle
	
	function updateCurrentLocation()
	{
		console.log('updated location');
		// Check is geolocation services are available...
		if (navigator.geolocation) {
			 navigator.geolocation.getCurrentPosition(function(position) {

				// position contains the latitute and longitude from the position.coords class
				 var position = {
					lat: position.coords.latitude,
					lng: position.coords.longitude,
					accuracy:position.coords.accuracy
				};
				 // Create LatLng Object to display a marker.
				currentPos = new google.maps.LatLng(position.lat, position.lng);
				 
				// remove previous markers if any
				// if(currentPosMarker !== null){
				//	 currentPosMarker.setMap(null);
				//	 circle.setMap(null);
				// }
				//set new Marker
				 currentPosMarker = new google.maps.Marker({
					 position: currentPos,
					 map: map,
					 title: 'Current Position',
					 icon: 'images/custom-marker1.png' // using a custom marker.
        		});
			
				 
				 // Creating an accuracy circle.
				 
				// var addCircle = function (map, coordinates , accuracy) {
				//	var circleOptions = {
				//	  center: coordinates,
				//	  clickable: false,
				//	  fillColor: "lightblue",
				//	  fillOpacity: 0.10,
				//	  map: map,
				//	  radius: accuracy,
				//	  strokeColor: "lightblue",
				//	  strokeOpacity: 0.3,
				//	  strokeWeight: 2
				//	};
 
    			//	 circle = new google.maps.Circle(circleOptions);
				//	 return circle;
  				//};
				 
				// var accuracy = position.accuracy;
				// addCircle(map,currentPos,accuracy);
				 
				 // Pan the map to move with the user.
				 map.setCenter(currentPos);
				 
				 var end = newRun.getEndingPoint();
				 // Check if the user is close to the destination (within 10m)
				 var distancetoEnd = google.maps.geometry.spherical.computeDistanceBetween(currentPos, end);
				 
				 // Call obj. mutator to set distancetoEnd
				 newRun.setDistancetoEnd(distancetoEnd);
				 
				 // Tell the user the remaining distance.
				 ////* Maybe I should remove this. Testing purposes only.
				 document.getElementById('QuitRunButton').innerHTML = "<b><I>QUIT RUN </I>-</b> Just "+distancetoEnd.toFixed(1)+"m to go! <I>Don't give up!</I>"
				 
				 // if the user has reached the destination, invoke quitRun() function
				 if(distancetoEnd<=10){
					 // simulate the click of the quit button.
					 document.getElementById('QuitRunButton').click(); 
					 var runComplete = true;
				 }else{
					 var runComplete = false;
				 }
				 
				 newRun.setRunCompletedStatus(runComplete);
			});
		}
	}
	
}


// Function checks user location to see if the Re-attempt is possible. i.e. if the user is at the proper starting location and enables the start run button.
function checkReAttemptLocationValidity(){
	
	document.getElementById('createDestinationButton').style.visibility = "hidden"
	document.getElementById('RUNButton').style.backgroundColor = "Navy";
	var runBtnInitialText = document.getElementById('RUNButton').innerHTML;
	document.getElementById('RUNButton').innerHTML = "Click to see if you are at the starting point!";
	
	document.getElementById('RUNButton').addEventListener('click', function(){
		
		navigator.geolocation.getCurrentPosition(function(position){

			// startingPosition contains the latitute and longitude from the navigator.geolocation class
			var userLocation = {
					lat: position.coords.latitude,
					lng: position.coords.longitude,
					accuracy: position.coords.accuracy
					};
			var start = newRun.getStartingPoint();
			var userPositionLatLng = new google.maps.LatLng(userLocation.lat, userLocation.lng);
			var checkDistance = google.maps.geometry.spherical.computeDistanceBetween(userPositionLatLng, start);
			
			if(checkDistance<20){
				// The user is close enough to the starting position to start the run.
				// Change the Buttons to initiate a Run.
				document.getElementById('RUNButton').innerHTML = runBtnInitialText;
				document.getElementById('RUNButton').style.backgroundColor = "red";
				document.getElementById('RUNButton').addEventListener('click', function(){
					startRun();
				});
			}else{
				// Display error messege to the user and request him to retry from the actual starting position.
				displayMessage("Your are "+checkDistance.toFixed(0)+"m away from the Saved Starting Point! Move to the proper starting point and press try again",5000);
				
				//Re load function
				checkReAttemptLocationValidity();
																	  
			}
			
		});
	});

}


// function to reload the page.
function reloadPage()
{
	window.location.href = "newRun.html";
}

// Invoking the CreateNewRun() method when the "Create a new Destination" Button is pressed.
document.getElementById('createDestinationButton').addEventListener('click', function()
{
	createNewRun();
});


// When the Start RUN button is pressed...
document.getElementById('RUNButton').addEventListener('click', function startRun()
{

	
	// Check if a destination has been set before starting the Run...
	if(dest_infoWindow !== null){
			
			//change body color back to white.
			document.body.style.backgroundColor='#fff'; 
		
			// Invoke runInProgress() to keep track of user position.
			runInProgress();
		
			//hide the starting location infoWindow.
			infoWindow.setMap(null);

			//hide current buttons.
			document.getElementById('newRunButtonDiv').style.display='none';

			// The size of the map is reduced to compensate for the new interface.
			document.getElementById('map').style.height='calc(100vh - 185px)';

			//Enabling the new interface StartedRunningDiv. (Quit button and timer)
			document.getElementById('startedRunningDiv').style.display='inline-block';

			//Resetting and calling the stopwatch function startTimer() to initiate.
			document.getElementById('TimerArea').value="00:00:00";
		
			newRun.startTimer('TimerArea'); 	//initiate stopwatch.

			//Toast Message to the user.
			displayMessage("Your Run will automatically end when you are close to your destination!",3500)

	}else{
		// Dispaly a messege asking the user to select a destination first.
		displayMessage("ERROR! Create a Destination before starting a new Run!!")
	}
});


//If the Quit RUN button is pressed... 
document.getElementById('QuitRunButton').addEventListener('click', function()
{
	
	
	// Stop tracking user location
	clearInterval(timerUpdateLocation);
	// Change Body Color
	document.body.style.backgroundColor='#1f2835';
	// display infoCard
	
	document.getElementById("clearAfterRunDiv").innerHTML="";
	document.getElementById('runInfoCard').style.display='block';
	
	
	
	// Display infomation about the run in the created material card...
	newRun.displayRunInfo("runInfo");
	

});

// Instructions for when the "save Run" button is pressed on the summary card.
document.getElementById('saveRunButton').addEventListener('click',function(){
	
	// Take the string given as the name of the run and pass it to the Run Object's name property.
	var nameStr = document.getElementById('nameRunInput').value;
	newRun.setRunName(nameStr);
	
	// Create a JSON compatible run object as newRun is too complex to be stringified.
	var JsonRunObject = new JsonRunObjectConstructor();
	
	// Send the JSON compatible Run Object to local storage
	saveRun(JsonRunObject);
	// Take user back to the home page.
	window.location.href = 'index.html';
	
});

