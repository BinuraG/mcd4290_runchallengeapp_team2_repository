
// Shared code needed by the code of all three pages.

// used to determine if the Run is an Re-Run. Set by the ViewRun page and used by the New Run page.
var isReRunCounter=false;
//Run Class Constructor
function Run()
{
	var name,
		dateTime,
		startingCoordinates,
		endingCoordinates,
		startingPoint, 
		endingPoint,
		timeTaken,
		timeInSec,
		runCompletedStatus,
		distanceRun,
		distancetoEnd,
		displacement,
		averageSpeed;
	
	this.setRunName = function(str){
		name = str;
	}
	this.getRunName = function(){
		if(name===null || name === undefined){
			name = "The Run has no name!";
			return name;
		}else{
		return name;
		}
	}
	this.setMapOfRun = function(map){
		mapOfRun = map;
	}
	this.getMapOfRun = function(){
		return mapOfRun;
	}
	var currentdate = new Date();
	dateTime =	currentdate.getDate() + "/"
				+ (currentdate.getMonth()+1)  + "/" 
				+ currentdate.getFullYear() + " @ "  
				+ currentdate.getHours() + ":"  
				+ currentdate.getMinutes(); 
	
	this.getDateTime = function(){
	return dateTime;
	}
	//Mutator method for startingPoint
	this.setStartingPoint = function(start){
		startingPoint = start;
	}
	// Accessor method for startingPoint.
	this.getStartingPoint = function(){
		return startingPoint;
	}
	
	//Mutator method for endingPoint
	this.setEndingPoint = function(end){
		endingPoint = end;
	}
	// Accessor method for startingPoint.
	this.getEndingPoint = function(){
		return endingPoint;
	}
	
	// Accessor and mutator for startingGeoLocationCoords
	this.setStartingCoordinates = function(value){
		startingCoordinates = value;
	}
	this.getStartingCoordinates = function(){
		return startingCoordinates;
	}
	// Mutator for geolocation ending coordinates.
	this.setEndingCoordinates = function(coords){
		endingCoordinates = coords;
	}
	// Accessor for geolocation ending coordinates.
	this.getEndingCoordinates = function(){
		return endingCoordinates;
	}
	// Mutator for RunCompletedStatus Boolean;
	this.setRunCompletedStatus = function(bool){
		
		if(bool===true){
			runCompletedStatus="Yes";
		}else{
			runCompletedStatus="No";
		}
	}// Accessor for RunCompletedStatus Boolean;
	this.getRunCompletedStatus = function(){
		return runCompletedStatus;
	}
	
	// Mutator for displacement
	this.setDisplacement = function(d){
		displacement=d;
	}
	this.getDisplacement = function(){
		return displacement;
	}
	
	// Mutator and Accessor for distance to the end (distancetoEnd)
	this.setDistancetoEnd = function(d){
		distancetoEnd = d;
	}
	this.getDistancetoEnd = function(){
	 	return distancetoEnd;
	}
	// Accessor for distance run by user (distanceRun)
	this.getDistanceRun = function(){
		distanceRun = (displacement-distancetoEnd);
		return distanceRun;
	}
	
	this.getAverageSpeed = function(){
		var distanceRun = this.getDistanceRun();
		averageSpeed = (distanceRun/timeInSec);
		return averageSpeed;
	}
	
	//Stopwatch Function
	/* The timer is started when the function is called and continues until the quitRunButton
	* (in NewRun.html) is pressed. 
	* Displays to the textArea 'TimerArea' in NewRun.html;
	*/
	this.startTimer = function(domElement){
		
		var disp = document.getElementById(domElement),
		stop = document.getElementById('QuitRunButton'),
		seconds = 0, minutes = 0, hours = 0, t;

		function add() {
			seconds++;
			if (seconds >= 60) {
				seconds = 0;
				minutes++;
				if (minutes >= 60) {
					minutes = 0;
					hours++;
				}
			}	
			// create a Time display string
			timeTaken = (hours ? (hours > 9 ? hours: "0" + hours) :"00")+":"+ 
				(minutes ? (minutes > 9 ? minutes: "0" + minutes) :"00")+":"+ 
				(seconds > 9 ? seconds: "0" + seconds);
			// calculate the time in seconds...
			timeInSec = seconds+(minutes*60)+(hours*3600);
			// Output time.
			disp.value = timeTaken;
			timer();
		}
		
		function timer(){
			t = setTimeout(add, 1000);
		}
		timer();
		// stop timer
		stop.addEventListener('click', function() {
			clearTimeout(t);
			
		});
		
	}
	// Accessor method to get Total run time (timeTaken)
	this.getTimeTaken = function(){
		return timeTaken;
	}
	this.getTimeInSeconds = function(){
		return timeInSec;
	}
	// public fuction to display Run infomation to a passed div element.
	
	this.displayRunInfo = function(divName){
	
		var distanceRun = this.getDistanceRun();
		var avgSpeed = this.getAverageSpeed();
		document.getElementById(divName).innerHTML+=		
		'RUN <b>SUMMARY <i class="material-icons">directions_run</i><i class="material-icons">flag</i></b><br>'+dateTime+
			"<br><ul>"+
			//"<li><b>Starting Coordinates:</b>    "+newRun.startingPoint+"</li>"+
			//"<li><b>Destination Coordinates:</b> "+newRun.endingPoint+"</li>"+
			"<li><b>Total Run Time:</b> "+timeTaken+"</li>"+
			"<li><b>Run Completed?:</b> "+runCompletedStatus+"</li>"+
			"<li><b>Distance Run:</b> "+distanceRun.toFixed(2)+" m</li>"+
			"<li><b>Average Speed:</b>"+avgSpeed.toFixed(2)+" m/s</li></ul>";
	}
} // end of Run constructor

/* Constructor class to create a JSON-object of the Run class. 
* JSON does not support complex objects therefore a simpler object  
* is created from this function.
*/
var JsonRunObjectConstructor =function()
{	
	
	this.name = newRun.getRunName();
	this.dateTime = newRun.getDateTime();
	this.timeTaken = newRun.getTimeTaken();
	this.distanceRun = newRun.getDistanceRun().toFixed(2);
	this.runCompletedStatus = newRun.getRunCompletedStatus();
	this.averageSpeed = newRun.getAverageSpeed().toFixed(2);
	this.distance = newRun.getDistancetoEnd().toFixed(2);
	
	// Starting Coordinates
	var sC = newRun.getStartingCoordinates();
	
	this.starting_lat = sC.lat;
	this.starting_lng = sC.lng;
	
	// ending Coordinates
	var eC = newRun.getEndingCoordinates();
	
	this.ending_lat = eC.lat;
	this.ending_lng = eC.lng;
}



/* Function to pass any given JSON-compatible Run Object to the local Storage. 
* INPUT: runObj - this instance must be constructed from the class JsonRunObject */
function saveRun(runObj)
{
	var APP_PREFIX = "monash.mcd4290.bin.runChallengeApp.";
	// Check if the browser supports local storage
	if (typeof(Storage) !== undefined){
		
		// Check if the program is saving for the first time or not
		if (localStorage.getItem(APP_PREFIX+'runIndex')===null){	
			// Initialize the local storage for further use.
			var runIndex = 0;
			localStorage.setItem(APP_PREFIX+'runIndex', runIndex);
		}
		
		// Obtain the run index number.
		var runIndex = localStorage.getItem(APP_PREFIX+'runIndex');
		// Stringify the run object.
		//var JSONrunObj = new JSON.stringify(runObj);
		// Store the JSON run object in local storage.
		localStorage.setItem(APP_PREFIX+runIndex, JSON.stringify(runObj));
		
		// Iterate the run index number
		runIndex++;
		
		// Store it.
		localStorage.setItem(APP_PREFIX+'runIndex',runIndex);
		
		
	} else {
		alert("Your browser does not support local storage. Update or change browser.");
	}
}


// Function to retrieve any passed Run Object index Number from the local Storage.
// The returned object is already parsed into a normal JS object.
function getRun(runIndex){
	
	var APP_PREFIX = "monash.mcd4290.bin.runChallengeApp.";
	
	// Check if the browser supports local storage
	if (typeof(Storage) !== undefined){
		
		var retrievedRunObject = localStorage.getItem(APP_PREFIX+runIndex);
		
		return JSON.parse(retrievedRunObject);
				
	} else {
		alert("Your browser does not support local storage. Update or change browser.");
	}
	
	
}



// Function in shared js file to switch to the viewRunPage when a specific Run Card is pressed in the index.html
function createViewPage(i)
{	
	// Set the current index in use to the local storage as .currentIndex so that it can be retrieved from the ViewRunPage.js script.
	var APP_PREFIX = "monash.mcd4290.bin.runChallengeApp.";
	var currentIndex = i;
	localStorage.setItem(APP_PREFIX+'.currentIndex',currentIndex);
}

// This function displays the given message String as a "toast" message at
// the bottom of the screen.  It will be displayed for 2 second, or if the
// number of milliseconds given by the timeout argument if specified.
function displayMessage(message, timeout)
{
    if (timeout === undefined)
    {
        // Timeout argument not specifed, use default.
        timeout = 2000;
    } 

    if (typeof(message) == 'number')
    {
        // If argument is a number, convert to a string.
        message = message.toString();
    }

    if (typeof(message) != 'string')
    {
        console.log("displayMessage: Argument is not a string.");
        return;
    }

    if (message.length == 0)
    {
        console.log("displayMessage: Given an empty string.");
        return;
    }

    var snackbarContainer = document.getElementById('toast');
    var data = {
        message: message,
        timeout: timeout
    };
    if (snackbarContainer && snackbarContainer.hasOwnProperty("MaterialSnackbar"))
    {
        snackbarContainer.MaterialSnackbar.showSnackbar(data);
    }
}



