// Code for the Measure Run page.



// Global-scope variable declarations //
var map = null;
var startingPosition;
var end, start;
var infoWindow = null;
var dest_infoWindow = null; //destination infoWindow used in function createNewRun() requires gloabl scope intialization.
var directionsDisplay = null; // google.maps.DirectionsRender Object, used by function drawRunPath(). Requires gloabl scope.

// Map Initialisation callback.  Will be called when Maps API loads.
function initMap() 
{
	// Initialise map, centred on Colombo before getting user location.
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: 6.9271, lng: 79.8612},
		zoom: 18
	});
	
	// call to center the map to the user's current position.
	setStartPosition();
}


//function to set the map to the current user position and display and infoWindow.
function setStartPosition()
{
	// This is will get the current user location and center the map to it and inform the user
	// using an info window.
	infoWindow = new google.maps.InfoWindow({map: map});

	if (navigator.geolocation) {
		 navigator.geolocation.getCurrentPosition(function(position) {

		// startingPosition contains the latitute and longitude from the position.coords class
			
			 startingPosition = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
		};
		 
		// Inform the user position has been found.
		infoWindow.setPosition(startingPosition);
		infoWindow.setContent('You are here!');

		// center the map to the current position.
		map.setCenter(startingPosition); 
			 
		//..........................
		//createNewRun();	
			 //////////////////////////////////////
			 
			 
		}, function() {
			handleLocationError(true, infoWindow, map.getCenter());
		  });
	} else {
	  // Browser doesn't support Geolocation
	  handleLocationError(false, infoWindow, map.getCenter());
	}
	// show errors if geolocation fails
	function handleLocationError(browserHasGeolocation, infoWindow, startingPosition) {
		infoWindow.setPosition(startingPosition);
		infoWindow.setContent(browserHasGeolocation ?
							  'Error: The Geolocation service failed.' :
							  'Error: Your device doesn\'t support geolocation.');
  	}	
}

/*Function to create a new destination within 60m and 150m of the user location.
* calculates displacement between the two points, and sets down an infoWIndow at the destination.
*/
function createNewRun()
{
	var properDistance = false // jump starting the while loop
	// checking if there are any previous end destination markers open from previous calls to createNewRun().
	if(dest_infoWindow !== null){
		// Removing that marker before creating a new destination.
		dest_infoWindow.setMap(null);
	}
	
	
	
	while (properDistance===false){
		
			// assigning a LatLng Object to the startingPosition coords.
			start = new google.maps.LatLng(startingPosition.lat, startingPosition.lng);
			// creating a destination LatLng Object.
			end = new google.maps.LatLng((startingPosition.lat+(Math.random()*0.001)),(startingPosition.lng+(Math.random()*0.001)) );
			//Calculating the distance between the two positions.
			var distance = google.maps.geometry.spherical.computeDistanceBetween(start, end);
			
			//Check if the generated position falls within the requirements.
			if(distance>60 && distance<150){
				properDistance=true;	// set the boolean counter to true.
			}	
		}
		
		// display a new InfoWindow at the destination with the displacement infomation.
		dest_infoWindow = new google.maps.InfoWindow({
			map: map,
			position: end,
			content: '<div style="yellow">Your Destination<br>('+distance.toFixed(0)+'m away)</div>'
		});

	//Invoke function drawRunPath to request walking directions from google.
	drawRunPath(start,end);
}


// Function to render the selected path joining the two destinations.
// A request must be made from Google services.
function drawRunPath (start,end)
{
	//check for any paths drawn before, if there are any previous paths, remove them.
	if(directionsDisplay !== null){
		directionsDisplay.setMap(null);
	}
	
	// creating a request object to be sent to google services.
	var requestPath = {
		origin: start,
		destination: end,
		travelMode: google.maps.TravelMode.WALKING
	};
	
	// API obj.s
	var directionsService = new google.maps.DirectionsService();
	directionsDisplay = new google.maps.DirectionsRenderer(
		{preserveViewport: true}); // set to stop map from zooming or panning once directions are rendered.
	
	// Request a path from start -> end, if recieved from google, draw the path onto the Map.
	directionsService.route(requestPath, function(response, status) {
		if(status=== google.maps.DirectionsStatus.OK){
			directionsDisplay.setDirections(response);
			directionsDisplay.setMap(map);
			
//			var heading = new google.maps.geometry.spherical.computeHeading(start, end);
//			console.log(heading);
//			map.setHeading(heading);
//			map.setTilt(45)
		}else { 	//display error
			alert("Directions Request Failed!");
		}
	});
}

// FUnction called when the start Run button is pressed. Calculates the user location continously
// and displays it as a marker on the map.
var timerUpdateLocation;
function runInProgress(){
	
	//update the current location every 2s.
	timerUpdateLocation = setInterval(updateCurrentLocation, 2000); 
	var currentPos;
	var currentPosMarker=null;
	
	function updateCurrentLocation()
	{
		console.log('updated location');
		// Check is geolocation services are available...
		if (navigator.geolocation) {
			 navigator.geolocation.getCurrentPosition(function(position) {

				// position contains the latitute and longitude from the position.coords class
				 var position = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				 // Create LatLng Object to display a marker.
				currentPos = new google.maps.LatLng(position.lat, position.lng);
				 
				// remove previous markers if any
				 if(currentPosMarker !== null){
					 currentPosMarker.setMap(null);
				 }
				//set new Marker
				 currentPosMarker = new google.maps.Marker({
					 position: currentPos,
					 map: map,
					 title: 'Current Position'
        		});
				 
				 // Check if the user is close to the destination (within 10m)
				 var distancetoEnd = google.maps.geometry.spherical.computeDistanceBetween(currentPos, end);
				 
				 // Tell the user the remaining distance.
				 ////* Maybe I should remove this. Testing purposes only.
				 document.getElementById('QuitRunButton').innerHTML = "<b><I>QUIT RUN </I>-</b> Just "+distancetoEnd.toFixed(1)+"m to go! <I>Don't give up!</I>"
				 
				 // if the user has reached the destination, invoke quitRun() function
				 if(distancetoEnd<=10){
					 quitRun();
				 }
			});
		}
	}
	
	}



// Invoking the CreateNewRun() method when the "Create a new Destination" Button is pressed.
document.getElementById('createDestinationButton').addEventListener('click', function(){
	createNewRun();
});


// When the Start RUN button is pressed...
document.getElementById('RUNButton').addEventListener('click', function() {

	// Check if a destination has been set before starting the Run...
	if(dest_infoWindow !== null){
			
			// Invoke runInProgress() to keep track of user position.
			runInProgress();
		
			//hide the starting location infoWindow.
			infoWindow.setMap(null);

			//hide current buttons.
			document.getElementById('newRunButtonDiv').style.display='none';

			// The size of the map is reduced to compensate for the new interface.
			document.getElementById('map').style.height='calc(100vh - 185px)';

			//Enabling the new interface StartedRunningDiv. (Quit button and timer)
			document.getElementById('startedRunningDiv').style.display='inline-block';

			//Resetting and calling the stopwatch function startTimer() to initiate.
			document.getElementById('TimerArea').value="00:00:00";
			startTimer();	//initiate stopwatch.

			//Toast Message to the user.
			displayMessage("Your Run will automatically end when you are close to your destination!",4000)

	}else{
		// Dispaly a messege asking the user to select a destination first.
		displayMessage("ERROR! Create a Destination before starting a new Run!!")
	}
});


//If the Quit RUN button is pressed... 
document.getElementById('QuitRunButton').addEventListener('click', quitRun());
function quitRun()
{
	// Stop tracking user location
	clearInterval(timerUpdateLocation);
	
	//disable the inditerminate running bar
	document.getElementById('runningBar').style.display='none';
	
	var endLocation;
	// Get the location the user pressed the quit button.
	if (navigator.geolocation) {
		 navigator.geolocation.getCurrentPosition(function(position) {

			var currentLocation = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			 //Assign the current location as a LatLng Obj to quitLocation reference.
			endLocation = new google.maps.LatLng(currentLocation.lat, currentLocation.lng);
			 
			
		 });
	}
	
			// creating a destination LatLng Object.
		
}
